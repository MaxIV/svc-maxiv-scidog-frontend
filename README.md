# svc-maxiv-scidog-frontend

## SciDog - Scientific (Meta)Data Organizer GUI

The SciDog is used to store and configure metadata that should be recorded and
stored in SciCat. Each beamline is responsible for their own set of metadata
that they wish to record. They can add/remove their metadata variables through
SciDog [https://scidog.maxiv.lu.se](https://scidog.maxiv.lu.se).

Each set of metadata consists of an optional group, a human-readable name for
the variable, the device and attribute combination and optionally a unit
corresponding to the value.

## Available Scripts

In the project directory, you can run:

```
npm install
npm start
```

Runs the app in the development mode. Open
[localhost:3000](http://localhost:3000) to view it in the browser.

## Backend

The backend can be found
[here](https://gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-scidog-backend/).

## Deployment

It is automatically deployed in k8s through the gitlab-ci. Please update the
appVersion in the helm-chart as well as the version in the package.json. A
[develop version](https://scidog-test-develop.apps.okd.maxiv.lu.se/) will be
created when pushing to the develop branch. After updating the master branch,
add a tag with the current version to deploy to production.
