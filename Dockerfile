FROM harbor.maxiv.lu.se/dockerhub/library/node:lts-slim as build-step
WORKDIR /home/app
COPY package.json ./
RUN npm install --no-cache
COPY src src
COPY public public
RUN npm run build

# Stage 2
FROM harbor.maxiv.lu.se/dockerhub/library/nginx:1.21.1-alpine
COPY --from=build-step /home/app/build /usr/share/nginx/html
COPY assets/nginx.conf /etc/nginx/

EXPOSE 3000
ENTRYPOINT ["nginx","-g","daemon off;"]
