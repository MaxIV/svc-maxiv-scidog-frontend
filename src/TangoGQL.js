export  const getAllDevices = function (beamline, cb) {
    if (beamline === "finestbeams"){
        beamline = "finest"
    }

    const url = "https://taranta.maxiv.lu.se/" + beamline + "/db"
    const query = JSON.stringify({"query":"\n  query {\n    devices {\n      name\n    }\n  }\n  ","variables":{}})

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: query,
        credentials: 'include',
    };


    fetch(url, requestOptions)
    .then(response => response.json())
    .then(data => {cb(data);})
    .catch(error => console.log(error))

}


export const getAttributes = function (beamline, device, cb) {
    if (beamline === "finestbeams"){
        beamline = "finest"
    }

    const url = "https://taranta.maxiv.lu.se/" + beamline + "/db"
    const query = JSON.stringify({"query":"\n  query FetchAttributeNames($device: String!) {\n    device(name: $device) {\n      attributes {\n        name\n        unit\n      }\n    }\n  }\n  ","variables":{"device":device}})

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: query,
        credentials: 'include',
    };

    fetch(url, requestOptions)
    .then(response => response.json())
    .then(data => {cb(data);})
    .catch(error => console.log(error))
}
