import React, {Component} from 'react';
import './global.js'
import DataTable from 'react-data-table-component';
import {Delete} from './Delete.js';
import {Alert, DropdownButton, Dropdown, Button, InputGroup, FormControl, Form} from 'react-bootstrap';

export class Scidog extends Component {
    constructor(props){
        super(props);
        this.fetchData = this.fetchData.bind(this)

        this.conditionalRowStyles = [
            {
              when: row => row.toggleSelected,
              style: {
                backgroundColor: "#dff0ef",
              }
            }
        ];

        this.state = {
            data: [],
            configurations: [],
            configurationsOptions: {},
            activeConfiguration: "",
            shownConfiguration: "All",
            shownData: [],
            addNewConfiguration: false,
            editConfiguration: false,
            selectedRows: [],
            showPopUp: false,
            popUpVariant: "",
            popUpMessage: "",
        }

    }

    componentDidMount(){
        this.fetchData()
    }

    fetchData(){
        let beamline_url = '/api/beamline/' + this.props.beamline.toLowerCase()
        fetch(beamline_url)
        .then(response => response.json())
        .then(
            (data) =>{
                let shownData = []
                data.data.sort((a, b) => a.Group.localeCompare(b.Group));
                for (let k = 0; k < data.data.length; k++) {
                    data.data[k].Edit = <Button id={data.data[k]._id} variant="outline-secondary" size="sm" onClick={this.handleChange}>✏️</Button>
                    data.data[k].Delete = <Delete id={data.data[k]._id} name={data.data[k].Name} fetchData={this.fetchData}/>

                    let mapping = []
                    data.data[k].Mapping.forEach(m => {
                        mapping.push(<p value={m} > {m.oldValue + " -> " + m.newValue }</p>);
                    })

                    if(mapping.length > 0) {
                    data.data[k].Mapping = mapping
                    } else {
                    data.data[k].Mapping = ""
                    }
                    if(this.state.configurations.length === 0 ) {
                        shownData = data.data
                    } else if (this.state.shownConfiguration === "All"){
                        shownData = data.data
                    } else if (this.state.configurations[this.state.shownConfiguration].includes(data.data[k]._id)){
                        shownData.push(data.data[k])
                    }
                }
        
                this.setState({
                    data: data.data,
                    shownData: shownData
                });
                this.props.setData(data.data)
            }
        )

        let configurations_url = '/api/config/beamline/' + this.props.beamline.toLowerCase()
        fetch(configurations_url)
        .then(response => response.json())
        .then(
            (configurations) =>{
                let configurationsOptions = [<Dropdown.Item key={"all"} onClick={this.showConfiguration} >All</Dropdown.Item>]
                if (configurations.data.Configuration !== {}){
                    Object.keys(configurations.data.Configuration).map(key => configurationsOptions.push(<Dropdown.Item key={key} onClick={this.showConfiguration}>{key}</Dropdown.Item>))
                }

                configurationsOptions.push(<Dropdown.Divider key={"divider"}/>)
                configurationsOptions.push(<Dropdown.Item key={"new"} onClick={this.showConfiguration}>Add new</Dropdown.Item>)

                this.setState({
                    configurations: configurations.data.Configuration,
                    configurationsOptions: configurationsOptions,
                    activeConfiguration: configurations.data.ActiveConfig,
                });
            }
        )



    }

    handleChange = (e) => {
        let editID = ""
        const updatedData = this.state.shownData.map(item => {
            try {
                document.getElementById(item._id).innerHTML = "✏️"
            } catch (err) {
                if(!(err instanceof TypeError)){
                    console.log(err)
                }
            }
            if (e.target.id !== item._id) {
                return {
                    ...item,
                    toggleSelected: false
                };
            }
            if(!item.toggleSelected){
                document.getElementById(item._id).innerHTML = "↩️"
                editID = e.target.id
            }
            return {
              ...item,
              toggleSelected: !item.toggleSelected
            };
          });

          this.setState({shownData: updatedData});
          this.props.setEditID(editID)

  }

    showConfiguration = (e) => {
        this.setState({shownConfiguration: e.target.innerHTML})

        let shownData = []
        let addNewConfiguration = false
        let editConfiguration = false
        if(e.target.innerHTML === "All"){
            shownData = this.state.data
        } else if(e.target.innerHTML === "Add new"){
            shownData = this.state.data
            addNewConfiguration = true
        } else {
            for (let k = 0; k < this.state.data.length; k++) {
                if(this.state.configurations[e.target.innerHTML].includes(this.state.data[k]._id)){
                    shownData.push(this.state.data[k])
                }
            }
        }
    this.setState({shownData: shownData, addNewConfiguration: addNewConfiguration, editConfiguration: editConfiguration})
    }

    handleSelectedRowsChange = (e) => {
        this.setState({selectedRows: e.selectedRows})
    }

    modifyConfiguration = (e) => {
        e.preventDefault();
        let name = ""
        if(this.state.addNewConfiguration){
            name = e.target[1].value
            this.setState({shownConfiguration: name})
        }
        if(this.state.editConfiguration) {
            name = this.state.shownConfiguration  
        }
        let newConfigurations = this.state.configurations
        newConfigurations[name] = []

        for (let j = 0; j < this.state.selectedRows.length; j++) {
            newConfigurations[name].push(this.state.selectedRows[j]._id)
        }

        const configurations_url = "/api/config/beamline/" + this.props.beamline.toLowerCase()
        const body = {"Configuration": newConfigurations}

        this.setState({
            addNewConfiguration: false,
            editConfiguration: false,
        })

        this.sendConfigChange(configurations_url, body)
    }

    changeActiveConfig = () => {
        const configurations_url = "/api/config/active/" + this.props.beamline.toLowerCase()
        const body = {"ActiveConfig": this.state.shownConfiguration}

        this.sendConfigChange(configurations_url, body)
    }

    sendConfigChange = (url, body) => {
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body),
        };

        fetch(url, requestOptions)
        .then((response) => {
            let variant = "warning"
            if(response.ok){
                variant = "success"
            }
            response.json().then(data => {
                this.popUp(variant, data.message);
            })
        });

    }

    editConfig = () => {
        let shownData = this.state.data
        let selectedRows = this.state.shownData
        this.setState({editConfiguration: true, shownData: shownData, selectedRows: selectedRows})
    }

    deleteConfig = () => {
        let newConfigurations = Object.assign({}, this.state.configurations)
        delete newConfigurations[this.state.shownConfiguration]
        this.setState({shownConfiguration: "All"})

        const configurations_url = "/api/config/beamline/" + this.props.beamline.toLowerCase()
        const body = {"Configuration": newConfigurations}

        this.sendConfigChange(configurations_url, body)
    }

    popUp = (variant, message) => {
        this.setState({upload: false, showPopUp: true, popUpVariant: variant, popUpMessage: message});
        setTimeout(() => {this.setState({showPopUp: false})}, 5000)
        if(variant === "success"){
            this.fetchData()
        }
    }


    render() {
        return (
                <div className="Scidog">
                    <Form onSubmit={this.modifyConfiguration}>
                        <h4>Scientific Metadata Configuration</h4>
                        <div>
                            Show configuration:&nbsp;
                            <DropdownButton id="dropdown-basic-button" title={this.state.shownConfiguration}>
                                {this.state.configurationsOptions}
                            </DropdownButton>
                            {!["All", "Add new"].includes(this.state.shownConfiguration) &&
                                <div id="EditDeleteConfigs">
                                    &nbsp;
                                    <Button id={"edit"} variant="outline-secondary" size="sm" onClick={this.editConfig}>✏️</Button>
                                    &nbsp;
                                    <Button id={"delete"} variant="outline-secondary" size="sm" onClick={() => { if (window.confirm('Are you sure you wish to delete the configuration ' + this.state.shownConfiguration + '?')) this.deleteConfig()}}>❌</Button>
                                </div>
                            }
                            {this.state.addNewConfiguration &&
                            <div>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Text id="inputGroup-sizing-default">Name</InputGroup.Text>
                                        <FormControl required
                                            aria-label="Default"
                                            aria-describedby="inputGroup-sizing-default"
                                        />
                                    </InputGroup>
                            </div>
                            }
                        </div>
                        <DataTable
                            // Header columns and data
                            noHeader
                            columns={global.columns}
                            data={this.state.shownData}
                            style={{ textAlign: "left" }}
                            //Layout
                            dense={true}
                            compact={true}
                            striped={true}
                            pagination={true}
                            paginationPerPage={25}
                            paginationRowsPerPageOptions={[25, 50, 100]}
                            conditionalRowStyles={this.conditionalRowStyles}

                            selectableRows={this.state.addNewConfiguration || this.state.editConfiguration}
                            selectableRowsHighlight={this.state.addNewConfiguration || this.state.editConfiguration}
                            selectableRowSelected={(row) => this.state.selectedRows.indexOf(row) > -1}
                            onSelectedRowsChange={this.handleSelectedRowsChange}

                        />
                        <div>
                            {this.state.editConfiguration && <Button type="submit" disabled={this.state.selectedRows.length === 0}>Update Configuration</Button>}
                            {this.state.addNewConfiguration && <Button type="submit" disabled={this.state.selectedRows.length === 0}>Add {this.state.selectedRows.length} entries in new configuration</Button>}
                            {!this.state.addNewConfiguration && !this.state.editConfiguration && 
                                <p>
                                    Active Configuration: {this.state.activeConfiguration} <br />
                                    {this.state.activeConfiguration !== this.state.shownConfiguration &&
                                        <Button onClick={this.changeActiveConfig}>Make '{this.state.shownConfiguration}' active</Button>
                                    }
                                </p>
                            }
                        </div>
                    </Form>
                    {this.state.showPopUp &&
                        <div id="popup">
                            <Alert key="popup" variant={this.state.popUpVariant}>
                                {this.state.popUpMessage}
                            </Alert>
                        </div>
                    }
                </div>
        );
    }
}
