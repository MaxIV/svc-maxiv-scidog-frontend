import React, {Component} from 'react';
import './global.js';
import {Alert, Button, Form, Row, Col, Tooltip, OverlayTrigger} from 'react-bootstrap';
import CreatableSelect from 'react-select/creatable';
import Select from 'react-select';
import {getAllDevices, getAttributes} from './TangoGQL.js'
import Toggle from 'react-toggle';

require("react-toggle/style.css");

export class AddMeta extends Component {
    constructor(props){
        super(props);

	var defaultBulk = [
	    {
		"Group": "",
		"Name": "Double Digit",
		"Device": "sys/tg_test/1",
		"Attribute": "double_scalar",
		"Unit": "",
		"Mapping": [
		    { "oldValue": 5, "newValue": 10 }
		]
	    }
	]

        this.state = {
            groupChecked: false,
            groupOptions: [],
            groupSelected: "",
            deviceOptions: [],
            deviceSelected: "",
            attributeOptions: [],
            attributeSelected: "",
            nameSelected: "",
            unitSelected: "",
            submitDisabled: true,
            addMetaWarning: false,
            showPopUp: false,
            popUpVariant: "",
            popUpMessage: "",
            mappingInputs: [],

	    bulkAdd: false,
	    bulk: JSON.stringify(defaultBulk, null, "  "),
        }

        this.addMetatoDB = this.addMetatoDB.bind(this);
        this.addBulkToDB = this.addBulkToDB.bind(this);
        this.formatBulk = this.formatBulk.bind(this);
    }
    componentDidMount() {
        getAllDevices(this.props.beamline.toLowerCase(), (data=>{this.setDevices(data.data.devices);}))
    }

    componentDidUpdate(prevProps, prevState) {

        if(!this.state.groupChecked && this.props.data.length > 0){
            let groupOptions = [{value: "", label: "No group"}]
            let groupList = []

            for (let k = 0; k < this.props.data.length; k++) {
                if (this.props.data[k].Group !== "" && !groupList.includes(this.props.data[k].Group)){
                    groupOptions.push({value: this.props.data[k].Group, label: this.props.data[k].Group})
                    groupList.push(this.props.data[k].Group)
                }
            }
            this.setState({groupOptions: groupOptions})
            this.setState({groupChecked: true})
        }
        if(this.state.deviceSelected !== "" && this.state.attributeSelected !== "" && this.state.addMetaWarning === false){
            if(this.state.submitDisabled){
                this.setState({submitDisabled: false})
            }
        } else if(this.state.submitDisabled === false){
            this.setState({submitDisabled: true})
        }

        if(prevProps.editID !== this.props.editID){
            if(this.props.editID !== ""){
                for (let k = 0; k < this.props.data.length; k++) {
                    if (this.props.editID === this.props.data[k]._id) {
                        if (this.props.data[k].Mapping !== "") {
                            this.props.data[k].Mapping.forEach( m => m.props.value)
                            this.setState({
                                mappingInputs: this.props.data[k].Mapping.map( m => m.props.value)
                            })
                        }
                        this.setState({ groupSelected: this.props.data[k].Group,
                                        nameSelected: this.props.data[k].Name,
                                        deviceSelected: this.props.data[k].Device,
                                        attributeSelected: this.props.data[k].Attribute,
                                        unitSelected: this.props.data[k].Unit,
                                       })
                        getAttributes(this.props.beamline.toLowerCase(), this.props.data[k].Device, (data=>{this.setAttributes(data.data.device.attributes);}))
                    }
                }
            } else {
                this.setState({ groupSelected: "",
                                nameSelected: "",
                                deviceSelected: "",
                                attributeSelected: "",
                                unitSelected: "",
                                mappingInputs:[],
                                addMetaWarning: false
                })
            }
        }
    }

    setDevices(data){
        let deviceOptions = []
        for (var k = 0; k < data.length; k++) {
            deviceOptions.push({value: data[k].name, label: data[k].name})
        }
        this.setState({deviceOptions: deviceOptions})
    }

    setAttributes(data){
        let attributeOptions = []
        if (data !== null){
            for (var k = 0; k < data.length; k++) {
                attributeOptions.push({value: data[k].name, label: data[k].name, unit: data[k].unit})
            }

        }
        this.setState({attributeOptions: attributeOptions})

    }

    handleChangeGroup = (group) => {
        if(group){
            this.setState({groupSelected: group.value})
        }  else {
            this.setState({groupSelected: ""})
        }
    };

    handleChangeDevice = (device) => {
        if(device){
            this.setState({deviceSelected: device.value})
            getAttributes(this.props.beamline.toLowerCase(), device.value, (data=>{this.setAttributes(data.data.device.attributes);}))
        } else {
            this.setState({deviceSelected: ""})
            this.setState({attributeOptions: []})
        }
    };

    handleChangeAttribute = (attribute) => {
        if(attribute){
            this.setState({attributeSelected: attribute.value})
            this.setState({unitSelected: attribute.unit})
            this.checkDeviceAttribute(this.state.deviceSelected, attribute.value)
        } else {
            this.setState({attributeSelected: ""})
            this.setState({unitSelected: ""})

        }
    };

    toggleBulk = (event) => {
	this.setState({bulkAdd: (this.state.bulkAdd === false)})
    };

    handleChangeBulk = (event) => {
	this.setState({bulk: event.target.value})
    };

    handleMappingInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...this.state.mappingInputs];
        list[index][name] = value;
        this.setState({mappingInputs: list});
    };

    handleRemoveClick = index => {
        const list = [...this.state.mappingInputs];
        list.splice(index, 1);
        this.setState({mappingInputs: list});
    };

    handleAddClick = () => {
        this.setState({mappingInputs: [...this.state.mappingInputs, { oldValue: "", newValue: "" }]});
    };

    checkDeviceAttribute(device, attribute) {
        let addMetaWarningNotChanged = true
        for (var k = 0; k < this.props.data.length; k++) {
            if (this.props.data[k].Device === device && this.props.data[k].Attribute === attribute && this.props.data[k]._id !== this.props.editID){
                addMetaWarningNotChanged = false
                this.setState({addMetaWarning: true})
            }
        }
        if(addMetaWarningNotChanged && this.state.addMetaWarning){
            this.setState({addMetaWarning: false})
        }
    }

    addMetatoDB(e) {
        e.preventDefault();

        this.setState({nameSelected: e.target[1].value})
        this.setState({unitSelected: e.target[4].value})

        let unit = e.target[4].value
        if(unit === "No unit"){
            unit = ""
        }

        const convertValue = (value) => {
            return !(isNaN(value)) ? Number(value) : value;
        }

        let mapping = this.state.mappingInputs;
        mapping.forEach(m => {
            m.oldValue = convertValue(m.oldValue);
            m.newValue = convertValue(m.newValue);
        })

        let entry = {
            Beamline: this.props.beamline.toLowerCase(),
            Group: this.state.groupSelected,
            Name: e.target[1].value,
            Device: this.state.deviceSelected,
            Attribute: this.state.attributeSelected,
            Unit: unit,
            Mapping: mapping
        }

        let url = "/api/meta"
        let requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(entry)
        };

        if(this.props.editID !== ""){
            url = "/api/meta/" + this.props.editID
            requestOptions.method = 'PUT'
        }

        fetch(url, requestOptions)
        .then((response) => {
            let variant = "warning"
            if(response.ok){
                variant = "success"
            }
            response.json().then(data => {
                this.popUp(variant, data.message);
            })
        });
    }

    addBulkToDB(e) {
	e.preventDefault()

	var bulk
	try {
	    bulk = JSON.parse(this.state.bulk)
	} catch(error) {
	    var errorMessage = error.toString()
	    this.popUp("warning", errorMessage)
	    return
	}

	bulk.map(record => {
	    record.Beamline = this.props.beamline.toLowerCase()
	    return record
	})

	let url = "/api/meta-bulk"
	let requestOptions = {
	    method: 'POST',
	    headers: { 'Content-Type': 'application/json' },
	    body: JSON.stringify(bulk),
	}

	fetch(url, requestOptions)
	.then((response) => {
            let variant = "warning"
            if(response.ok){
                variant = "success"
            }
            response.json().then(data => {
                this.popUp(variant, data.message);
            })
	});
    }

    formatBulk(e) {
	e.preventDefault()

	try {
	    var bulk = JSON.parse(this.state.bulk)
	} catch(error) {
	    this.popUp("warning", error.toString());
	    return
	}

	this.setState({bulk: JSON.stringify(bulk, null, "  ")})
    }

    popUp(variant, message){
        this.setState({upload: false, showPopUp: true, popUpVariant: variant, popUpMessage: message});
        setTimeout(() => {this.setState({showPopUp: false})}, 5000)
        if(variant === "success"){
            window.location.reload();
        }
    }

    mappingTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            (Optional) mapping allows to change the shown output value of the attribute to make it more readable. For example,
            if you expect the state of a device to be either 4 or 5 (old values), but it corresponds
            to insert and extract (new values), you can map these values here.
        </Tooltip>
    );


    render() {
        let groupValue = null
        if(this.state.groupSelected !== ""){
            groupValue = {value: this.state.groupSelected, label: this.state.groupSelected}
        }
        let deviceValue = null
        if(this.state.deviceSelected !== ""){
            deviceValue = {value: this.state.deviceSelected, label: this.state.deviceSelected}
        }
        let attributeValue = null
        if(this.state.attributeSelected !== ""){
            attributeValue = {value: this.state.attributeSelected, label: this.state.attributeSelected}
        }

        return (
            <>
                <div className="AddMeta">
                    {this.props.editID === "" ?
                    <h4>Add Metadata</h4>
                    :
                    <h4>Add Metadata - Editing entry {this.state.nameSelected} </h4>
                    }

		    <Toggle
			id='bulk-toggle'
                        checked={this.state.bulkAdd}
                        name='showBulk'
                        value='yes'
                        onChange={this.toggleBulk}
		    />
		    <label id="bulk-toggle-label" htmlFor='bulk-toggle'>Bulk mode</label>

		    {this.state.bulkAdd ?
		     <>
			 <p>Name, Device and Attribute are mandatory</p>
			 <Form onSubmit={this.addBulkToDB}>
                             <Form.Group controlId="formBulk">
				 <textarea
				     className="bulkEditor"
				     value={this.state.bulk}
				     onChange={this.handleChangeBulk}
				 />
				 <br/>
				 <Button variant="secondary" onClick={this.formatBulk} type="button">Format</Button>
				 &nbsp;
				 <Button variant="primary" type="submit">Submit</Button>
			     </Form.Group>
			 </Form>
		     </>
		     :
                     <Form onSubmit={this.addMetatoDB}>
                         <Form.Group controlId="formGroup">
                             <Form.Label>Group</Form.Label>
                             <CreatableSelect
                                 isClearable
                                 onChange={this.handleChangeGroup}
                                 options={this.state.groupOptions}
                                 value={groupValue}
                                 placeholder={"Type to search or add a new group"}
                             />
                         </Form.Group>
                         <Form.Group controlId="formName">
                             <Form.Label>Name</Form.Label>
                             <Form.Control required type="name" defaultValue={this.state.nameSelected} placeholder="Custom name" />
                         </Form.Group>
                         <Form.Group controlId="formDevice">
                             <Form.Label>Device</Form.Label>
                             <Select
                                 isClearable={true}
                                 isSearchable={true}
                                 onChange={this.handleChangeDevice}
                                 options={this.state.deviceOptions}
                                 value={deviceValue}
                                 placeholder={"Type to search"}
                             />
                         </Form.Group>
                         <Form.Group controlId="formAttribute">
                             <Form.Label>Attribute</Form.Label>
                             <Select
                                 isClearable={true}
                                 isSearchable={true}
                                 onChange={this.handleChangeAttribute}
                                 options={this.state.attributeOptions}
                                 value={attributeValue}
                                 placeholder={"Type to search"}
                             />
                             {this.state.addMetaWarning &&
                              <Form.Text className="addMetaWarning">
                                  Warning: this device/attribute pair is already in the database!
                              </Form.Text>
                             }
                         </Form.Group>
                         <Form.Group controlId="formUnit">
                             <Form.Label>Unit</Form.Label>
                             <Form.Control type="unit" defaultValue={this.state.unitSelected}/>
                         </Form.Group>
                         <Form.Group controlId="formMapping">
                             {this.state.mappingInputs.map((x, i) => {
                                 return (
                                     <Row className="mb-3" key={"formMapping"+i.toString()}>
					 <Form.Group as={Col} controlId={"formMappingOld"+i}>
					     <Form.Label>Map - Old Value</Form.Label>
					     <Form.Control
						 name="oldValue"
						 placeholder="Old value"
						 value={x.oldValue}
						 onChange={e => this.handleMappingInputChange(e, i)}
						 required
					     />
					 </Form.Group>
					 <Form.Group as={Col} controlId={"formMappingNew"+i}>
					     <Form.Label>Map - New Value</Form.Label>
					     <Form.Control
						 name="newValue"
						 placeholder="New value"
						 value={x.newValue}
						 onChange={e => this.handleMappingInputChange(e, i)}
						 required
					     />
					 </Form.Group>
					 <Form.Group controlId="formMapping">
                                             {this.state.mappingInputs.length && <Button className="m-1" onClick={() => this.handleRemoveClick(i)}>Remove mapping</Button>}
					 </Form.Group>
                                     </Row>
                                 );
                             })}
                             <OverlayTrigger
                                 placement="right"
                                 delay={{ show: 250, hide: 400 }}
                                 overlay={this.mappingTooltip}
                             >
                                 <Button className="m-1" onClick={this.handleAddClick}>Add new mapping</Button>
                             </OverlayTrigger>
                         </Form.Group>
                         <br/>
                         <Button variant="primary" type="submit" disabled={this.state.submitDisabled}>
                             {this.props.editID === "" ?
                              <>Submit</>
                              :
                              <>Update</>
                             }
                         </Button>
                     </Form>
		    }
                </div>
                {this.state.showPopUp &&
                    <div id="popup">
                        <Alert key="popup" variant={this.state.popUpVariant}>
                            {this.state.popUpMessage}
                        </Alert>
                    </div>
                }
            </>
        );
    }
}
