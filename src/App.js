import React, {Component} from 'react';
import logo from "./images/maxiv_logo.png";
import {Login} from "./Login";
import {Scidog} from "./Scidog";
import {AddMeta} from "./AddMeta";
import { Button } from 'react-bootstrap';
import './global.js';
import Cookies from 'universal-cookie';
import {
    BrowserRouter as Router,
    Link
  } from "react-router-dom";
import {Catdog} from './Catdog'


  export default class App extends Component {
    constructor(props) {
        super(props);
        this.jwt = null;
        this.loggedIn = false

        try{
            //Get login from local storage
            if(localStorage.getItem("jwt")){
                this.jwt = localStorage.getItem("jwt");
                this.loggedIn = true;
            }
        } catch(error) {
            console.log(error)
        }

        const cookies = new Cookies();
        //Get login from cookies
        if(typeof(cookies.get("jwt")) != "undefined"){
            this.jwt = cookies.get("jwt")
            this.loggedIn = true;
        }

        if(this.jwt){
            this.checkAccessRights(this.jwt)
        }

        this.state = {
            beamline: "Please select a beamline",
            homepage: true,
            options: null,
            loginShown: false,
            loggedIn: this.loggedIn,
            accessRights: false,
            jwt: this.jwt,
            username: null,
            data: [],
            addMetaWarning: false,
            catdog: false,
            editID: "",
        };

        this.select = this.select.bind(this);
        this.openLogin = this.openLogin.bind(this);
        this.closeLogin = this.closeLogin.bind(this);
        this.loggingIn = this.loggingIn.bind(this);
        this.logout = this.logout.bind(this);
        this.setData = this.setData.bind(this);
        this.setEditID = this.setEditID.bind(this);
    }

    componentDidMount() {
        const pathname = window.location.pathname.replace("/","")

        for (var k = 0; k < global.beamline.length; k++) {
            if (global.beamline[k].toLowerCase() === pathname){
                this.setState(
                    {beamline: global.beamline[k],
                    homepage: false}
                )
            }
            else if ("catdog" === pathname) {
                this.setState({catdog: true})
            }
        }

        let options = [];
        for (var j = 0; j < global.beamline.length; j++) {
            let link = "/" + global.beamline[j].toLowerCase()
            options[j] = [<Link to={link} key={global.beamline[j]}>
                            <Button className="beamlineselect" value={global.beamline[j]} onClick={this.select}>{global.beamline[j]}</Button>
                          </Link>]
        }
        this.setState({options: options})
    }

    select(e) {
        this.setState(
            {beamline: e.target.value,
            homepage: false}
        )
        if(this.state.jwt){
            this.checkAccessRights(this.state.jwt)
        }
    }

    reloadPage() {
        window.location = window.location.origin
    }

    openLogin() {
        this.setState({loginShown: true})
    }

    closeLogin() {
        this.setState({loginShown: false})
    }

    loggingIn(username, jwt, remember) {
        this.setState({loggedIn: true,
                        jwt: jwt,
                        username: username,
        })

        this.checkAccessRights(jwt)

        if(remember){
            localStorage.setItem("jwt", jwt);
        }
        const cookies = new Cookies();
        cookies.set("jwt", jwt, { path: '/', sameSite: "none", secure: true, maxAge: 86400  });
    }

    logout() {
        this.setState({loggedIn: false,
                        jwt: null,
                        username: null})

        localStorage.removeItem("jwt");

        const cookies = new Cookies();
        cookies.remove("jwt");
    }

    setData(data) {
        this.setState({data: data})
    }

    setEditID(id) {
        this.setState({editID: id})
    }

    checkAccessRights(jwt) {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({"jwt": jwt}),
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        };

        fetch('https://auth.maxiv.lu.se/v1/decode', requestOptions)
        .then(response => response.json())
        .then(decoded => {
            this.setState({userGroups: decoded.groups, username: decoded.username})
            if(decoded.groups.includes(this.state.beamline.toLowerCase()) || decoded.groups.includes("KITS")){
                this.setState({accessRights: true})
            }
        })
        .catch(error => console.log(error))
    }


    render() {
        return (
            <>
            {!this.state.catdog ?
            <div className="App">
                <div className="title">
                    <img className="home" src={logo} alt="MAXIV_logo" onClick={() => this.reloadPage()}/>&nbsp;&nbsp; SciDog - {this.state.beamline}
                    <div className="home_right">
                        {this.state.loggedIn ?
                            <div><p id="loggedInAs">Logged in as <i>{this.state.username}</i>&nbsp; &nbsp; </p><Button variant="secondary" onClick={this.logout}>Log out</Button></div>
                            :
                            <Button variant="secondary" onClick={this.openLogin}>Login</Button>
                        }
                    </div>
                </div>

                <Login
                    loginShown={this.state.loginShown}
                    loginHide={this.closeLogin}
                    loggedIn={this.loggingIn}
                />

                {this.state.homepage ?
                    <div className="content2">
                        <Router>
                            {this.state.options}
                        </Router>
                    </div>
                    :
                    <div>
                        {this.state.loggedIn ?
                            <div>
                                {this.state.accessRights ?
                                    <div>
                                        <Scidog
                                            beamline={this.state.beamline}
                                            setData={this.setData}
                                            setEditID={this.setEditID}
                                        />
                                        <AddMeta
                                            beamline={this.state.beamline}
                                            data={this.state.data}
                                            editID={this.state.editID}
                                        />
                                    </div>
                                :
                                    <p>You don't have permission to see scans from this beamline</p>
                                }
                            </div>
                        :
                            <p>Please log in to access SciDog</p>
                        }
                    </div>
                }
            </div>
            :
            <Catdog />
            }
            <div id="footer"><div id="food" style={{top: Math.floor(Math.random() * 100) +"px", left: Math.floor(Math.random() * 100)+"%"}}></div></div>
            </>
        )
    }
}
