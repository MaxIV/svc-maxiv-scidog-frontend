import React, {Component} from 'react';

export class Catdog extends Component {
    render() {
        const catdogdiv = {
            margin: 0,
            padding: 0,
            height: "100vh",
            backgroundImage: "url('https://wallpapercave.com/wp/wp2386092.jpg')",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
        }
        const cat = {
            height: "100vh",
            width: "49%",
            float: "left",
            display: "block",
        }
        const catdog = {
            height: "100vh",
            position: "absolute",
            width: "2%",
            left: "49%",
            margin: "auto",
            display: "block",
        }
        const dog = {
            height: "100vh",
            width: "49%",
            float: "right",
            display: "block",
        }
        const toad = {
            backgroundImage: "url('https://static.wikia.nocookie.net/adventures-of-chris-and-tifa/images/8/85/Toad_Artwork_-_Mario_Party_6.png')",
            backgroundRepeat: "no-repeat",
            backgroundSize: "contain",
            backgroundPosition: "center",
            width: "10%",
            height: "30%",
            left: "45%",
            bottom: "0",
            margin: "auto",
            position: "absolute",
            display: "block",
            zIndex: 2,
        }

        return (
            <div style={catdogdiv}>
                <a href="https://scicat-test.maxiv.lu.se" style={cat}> </a>
                <a href="https://www.youtube.com/watch?v=QSFj4vEDZQw" style={catdog}> </a>
                <a href="https://scidog.maxiv.lu.se" style={dog}> </a>
                <a href="https://scanlog.maxiv.lu.se" style={toad}> </a>
            </div>
        );
    }
}
