import React, {Component} from 'react';
import './global.js';
import {Alert, Button} from 'react-bootstrap';

export class Delete extends Component {
    constructor(props){
        super(props);

        this.state = {
            showPopUp: false,
            popUpVariant: "",
            popUpMessage: ""

        }
        this.deleteEntry = this.deleteEntry.bind(this);
    }

    deleteEntry() {
        const delete_url = "/api/meta/" + this.props.id
        const requestOptions = {
            method: 'DELETE'
        };
        fetch(delete_url, requestOptions)
        .then((response) => {
            let variant = "warning"
            if(response.ok){
                variant = "success"
            } 

            response.json().then(data => {
                this.popUp(variant, data.message);
            })
        });
    }

    popUp(variant, message){
        this.setState({upload: false, showPopUp: true, popUpVariant: variant, popUpMessage: message});
        setTimeout(() => {this.setState({showPopUp: false})}, 5000)
        if(variant === "success"){
            this.props.fetchData()
        }
    }
    
    render() {
        return (
            <div>
                <Button variant="outline-secondary" size="sm" onClick={(e) => { if (window.confirm('Are you sure you wish to delete ' + this.props.name + '?')) this.deleteEntry(e) } }>❌</Button>
                {this.state.showPopUp &&
                    <div id="popup">
                        <Alert key="popup" variant={this.state.popUpVariant}>
                            {this.state.popUpMessage}
                        </Alert>
                    </div>
                }
            </div>

        );
    }
}
