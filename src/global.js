global.beamline = ["CoSAXS", "FemtoMAX", "FlexPES", "DanMAX", "Veritas", "MAXPEEM", "NanoMAX", "FinEstBeAMS", "Balder", "HIPPIE", "ForMAX", "SPECIES", "BioMAX", "MicroMAX", "BLOCH", "SoftiMAX"];

global.columns = [
    {"name": "Group", "selector": "Group", "sortable": false, "width": "130px", "compact": true, "wrap": true },
    {"name": "Name", "selector": "Name", "sortable": false, "width": "150px", "compact": true, "wrap": true },
    {"name": "Device", "selector": "Device", "sortable": false, "width": "200px", "compact": true, "wrap": true },
    {"name": "Attribute", "selector": "Attribute", "sortable": false, "width": "100px", "compact": true, "wrap": true },
    {"name": "Unit", "selector": "Unit", "sortable": false, "width": "50px", "compact": true, "wrap": true },
    {"name": "Mapping", "selector": "Mapping", "sortable": false, "width": "150px", "compact": true, "wrap": true },
    {"name": "Edit", "selector": "Edit", "sortable": false, "width": "40px", "compact": true, "wrap": true },
    {"name": "Delete", "selector": "Delete", "sortable": false, "width": "40px", "compact": true, "wrap": true },
]
