import React, { Component } from "react";
import {Modal} from "react-bootstrap";

export class Login extends Component {
    constructor(props){
        super(props);

        this.state = {
            user: null,
            failed: false,
        }

        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();

        const creds = {"username": e.target[1].value,
        "password": e.target[2].value}

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(creds),
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        };

        fetch('https://auth.maxiv.lu.se/v1/login', requestOptions)
        .then(response => response.json())
        .then(data => {
            this.setState({failed: false})
            this.props.loginHide()
            this.props.loggedIn(e.target[1].value, data.jwt, e.target[3].checked)
        })
        .catch(error => this.setState({failed: true}))
    }

    render() {
        return (
            <Modal
                show={this.props.loginShown}
                // onSubmit={this.onSubmit()}
                onHide={this.props.loginHide}
                >
                <div id="LoginForm">
                    <form onSubmit={this.onSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Sign in</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            {this.state.failed &&
                            <div className="alert alert-danger" role="alert">
                                Wrong username and/or password.
                            </div>
                            }
                            <div className="form-group">
                                <label>Username</label>
                                <input type="text" className="form-control" placeholder="Enter username" />
                            </div>

                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" className="form-control" placeholder="Enter password" />
                            </div>

                            <div className="form-group">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="rememberme" />
                                    <label className="custom-control-label" htmlFor="rememberme">Remember me</label>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <button type="submit" className="btn btn-primary btn-block">Submit</button>
                        </Modal.Footer>
                    </form>
                </div>
            </Modal>
        );
    }
}
